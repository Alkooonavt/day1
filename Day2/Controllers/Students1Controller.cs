﻿using Day2.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Day2.Controllers
{
    public class Students1Controller : Controller
    {
        // GET: Students1
        private readonly Students students; 
        public Students1Controller()
        {
            this.students = new Students();
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Title";
            return View(students.StudentsList);
        }
    }
}