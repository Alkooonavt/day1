﻿using Day2.Data;
using Day2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Day2.Controllers
{
    public class Students2Controller : Controller
    {
        private readonly Students students;
        public Students2Controller()
        {
            this.students = new Students();
        }
        public ActionResult Index()
        {
            return View(students.StudentsList);
        }

        // GET: Students2/Details/5
        public ActionResult Details(int id)
        {
            Student student = students.StudentsList.FirstOrDefault(s => s.Id == id);
            return View();
        }

        // GET: Students2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Students2/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Students2/Edit/5
        public ActionResult Edit(int ?id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = students.StudentsList.FirstOrDefault(x => x.Id == id);
            return View(student);
        }

        // POST: Students2/Edit/5
        [HttpPost]
        public ActionResult Edit(Student student)
        {
            return View(student);
        }

        // GET: Students2/Delete/5
        public ActionResult Delete(int ?id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = students.StudentsList.FirstOrDefault(x => x.Id == id);
            return View(student);
        }

        // POST: Students2/Delete/5
        [HttpPost]
        public ActionResult Delete(Student student)
        {
            try
            {
                students.StudentsList.Remove(student);
                   return RedirectToAction("Index");
            }
            catch
            {
                return View();  
            }
        }
    }
}
